# Opendoors

Use this package to build your drupal distro via drush make.
-> see documentation at https://www.drupal.org/project/opendoors

Find a fully built distro at https://github.com/Zebralog/opendoors.